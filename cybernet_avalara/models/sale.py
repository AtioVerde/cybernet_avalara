from odoo import models, fields, api, _
from odoo.osv import osv
from odoo.exceptions import UserError
from datetime import datetime
import requests
import base64
import json


class SaleOrder(models.Model):
    _inherit = ["sale.order"]

    is_avalara = fields.Boolean(compute="_compute_visible")
    sale_type = fields.Selection([("0", "Wholesale"), ("1", "Retail"), ("2", "Consumed"), ("3", "Vendor Use")])
    federal_tax = fields.Monetary("Federal Tax")
    state_tax = fields.Monetary("State Tax")
    county_tax = fields.Monetary("County Tax")
    city_tax = fields.Monetary("City Tax")
    unincorporated_tax = fields.Monetary("Unincorporated Tax")
    tax_line_ids = fields.Many2many("tax.line", "tax_sale_rel", "order_id", "tax_line_id", "Tax Line", ondelete="cascade")
    total_tax = total_amount_taxed_product = 0.0
    tax_document = {}
    invoice_document = {}
    customer_id = fields.Many2one("res.partner")
    invoice = []
    tax_line_list = []

    @api.onchange("order_line")
    def _compute_visible(self):
        self.is_avalara = self.env.company.id == self.env.user.company_name.id

    @api.model
    def _prepare_invoice(self):
        """
        Prepare the dict of values to create the new invoice for a sales order. This method may be
        overridden to implement custom invoice generation (making sure to call super() to establish
        a clean extension chain).
        """
        self.ensure_one()
        journal = (
            self.env["account.move"]
                .with_context(force_company=self.company_id.id, default_type="out_invoice")
                ._get_default_journal()
        )

        if not journal:
            raise UserError(
                _("Please define an accounting sales journal for the company %s (%s).")
                % (self.company_id.name, self.company_id.id)
            )

        invoice_vals = {
            "ref": self.client_order_ref or "",
            "type": "out_invoice",
            "narration": self.note,
            "currency_id": self.pricelist_id.currency_id.id,
            "campaign_id": self.campaign_id.id,
            "medium_id": self.medium_id.id,
            "source_id": self.source_id.id,
            "invoice_user_id": self.user_id and self.user_id.id,
            "team_id": self.team_id.id,
            "partner_id": self.partner_invoice_id.id,
            "partner_shipping_id": self.partner_shipping_id.id,
            "fiscal_position_id": self.fiscal_position_id.id or self.partner_invoice_id.property_account_position_id.id,
            "invoice_origin": self.name,
            "invoice_payment_term_id": self.payment_term_id.id,
            "invoice_payment_ref": self.reference,
            "transaction_ids": [(6, 0, self.transaction_ids.ids)],
            "invoice_line_ids": [],
            "federal_tax": self.federal_tax,
            "state_tax": self.state_tax,
            "county_tax": self.county_tax,
            "city_tax": self.city_tax,
            "unincorporated_tax": self.unincorporated_tax,
            "sale_type": self.sale_type,
            "tax_line_ids": [(6, 0, self.tax_line.ids)],
        }
        return invoice_vals

    @api.onchange("order_line", "partner_id", "sale_type")
    def get_ava_tax(self):
        """
        Compute the total amounts of the SO.
        """
        try:
            lines = []
            amount_untaxed = amount_tax = fed_t = sta_t = cou_t = cit_t = unin_t = 0.00
            for order in self:
                if "partner_id" in order:
                    self.customer_id = order["partner_id"].id

                line_details = order.order_line
                self.tax_line_list.clear()
                for line in line_details:
                    tax_ids = line.tax_id

                    for tax in tax_ids:
                        if tax.name == "AFC Tax":
                            lines.clear()
                            lines.append(
                                {
                                    "chg": line.price_subtotal,
                                    "sale": int(order.sale_type) if order.sale_type else "",
                                    "tran": int(line.tran_service_type.t_code) if line.tran_service_type.t_code else "",
                                    "serv": int(line.tran_service_type.s_code) if line.tran_service_type.s_code else "",
                                    "qty": int(line.product_uom_qty) if line.product_uom_qty else None,
                                }
                            )
                            self.invoice_document["itms"] = lines
                            value = self.get_tax_avalara(line.name)
                            self.total_tax += float(value[0])
                            fed_t += float(value[1])
                            sta_t += float(value[2])
                            cou_t += float(value[3])
                            cit_t += float(value[4])
                            unin_t += float(value[5])

                            self.total_amount_taxed_product += line.price_subtotal
                            amount_tax += float(value[0])
                        else:
                            amount_tax += line.tax_ids.amount
                    amount_untaxed += line.price_subtotal
                self.update(
                    {
                        "amount_untaxed": amount_untaxed,
                        "amount_tax": amount_tax,
                        "amount_total": amount_untaxed + amount_tax,
                        "federal_tax": fed_t,
                        "state_tax": sta_t,
                        "county_tax": cou_t,
                        "city_tax": cit_t,
                        "unincorporated_tax": unin_t,
                        "tax_line_ids": [[6, 0, self.tax_line_list]] if self.tax_line_list else None,
                    }
                )
                self.env.cr.commit()
                self._tax_amount_all()
        except Exception as e:
            raise osv.except_osv("", e)

    def _tax_amount_all(self):
        if self.total_tax != 0.0 and self.total_amount_taxed_product != 0.0:
            tax_amount = (self.total_tax / self.total_amount_taxed_product) * 100
            tax = self.env["account.tax"].search([("name", "=", "AFC Tax")])
            tax.write(
                {
                    "name": "AFC Tax",
                    "type_tax_use": "sale",
                    "amount": tax_amount,
                    "description": "Avalara for Communication Tax",
                }
            )
            self.env.cr.commit()

    def get_customer_detail(self):
        if self.customer_id:
            cus_model = self.env["res.partner"]
            cus_rec_id = cus_model.search([("id", "=", self.customer_id)]).id

            customer = cus_model.browse(cus_rec_id)
            if customer.customer_type:
                self.invoice_document["cust"] = int(customer.customer_type)

            location = {
                "city": customer.city,
                "st": self.env["res.country.state"].search([("id", "=", int(customer.state_id))]).code,
                "zip": customer.zip,
                "ctry": self.env["res.country"].search([("id", "=", int(customer.country_id))]).code,
            }
            self.invoice_document["bill"] = location
        else:
            raise osv.except_osv("", "Please Select Customer")

    def get_tax_avalara(self, p_name):
        if self.env.user.client_id and self.env.user.email and self.env.user.password:
            try:
                if self.env.user.client_id and self.env.user.email and self.env.user.password:
                    self.get_customer_detail()
                    self.invoice_document["date"] = str(datetime.now())
                    self.invoice_document["cmmt"] = False
                    self.invoice.append(self.invoice_document)
                    self.tax_document["inv"] = self.invoice
                    company_details = {
                        "bscl": int(self.env.user.business_class),
                        "svcl": int(self.env.user.service_class),
                        "frch": True if self.env.user.franchise == "true" else False,
                        "fclt": True if self.env.user.facilities == "true" else False,
                        "reg": True if self.env.user.regulated == "true" else False,
                    }
                    self.tax_document["cmpn"] = company_details

                    calculate_tax = requests.post(
                        "https://communications.avalara.net/api/v2/Afc/CalcTaxes",
                        headers={
                            "Content-Type": "application/json",
                            "api_key": str(
                                base64.b64encode((str(self.env.user.email) + ":" + str(self.env.user.password)).encode()),
                                "utf-8",
                            ),
                            "client_id": self.env.user.client_id,
                        },
                        data=json.dumps(self.tax_document),
                    )
                    transaction_type = json.loads(calculate_tax.text)
                    if len(transaction_type["inv"][0]["itms"][0]) != 0:
                        if "txs" in transaction_type["inv"][0]["itms"][0]:
                            taxes = transaction_type["inv"][0]["itms"][0]["txs"]
                            total_tax = 0
                            federal_t = 0
                            state_t = 0
                            county_t = 0
                            city_t = 0
                            unincorporated_t = 0
                            if self.tax_line:
                                self.update({"tax_line_ids": [[6, 0, []]]})
                                self.tax_line.unlink()
                            for tax in taxes:
                                lvl = None
                                if tax["lvl"] == 0:
                                    federal_t += tax["tax"]
                                    lvl = "Federal"
                                elif tax["lvl"] == 1:
                                    state_t += tax["tax"]
                                    lvl = "State"
                                elif tax["lvl"] == 2:
                                    county_t += tax["tax"]
                                    lvl = "County"
                                elif tax["lvl"] == 3:
                                    city_t += tax["tax"]
                                    lvl = "City"
                                else:
                                    unincorporated_t += tax["tax"]
                                    lvl = "Unincorporated"

                                new_tax = self.env["tax.line"].create(
                                    {
                                        "tax_id": tax["tid"],
                                        "customer_name": self.partner_id.name,
                                        "name": tax["name"],
                                        "product_name": p_name,
                                        "amount": tax["tax"],
                                        "tax_level": lvl,
                                        "date": datetime.now(),
                                    }
                                )
                                self.tax_line_list.append(new_tax.id)
                            total_tax = federal_t + state_t + county_t + city_t + unincorporated_t

                            return total_tax, federal_t, state_t, county_t, city_t, unincorporated_t
                        else:
                            return 0
                    else:
                        return 0
            except Exception as e:
                raise osv.except_osv("", e)
        else:
            raise osv.except_osv(" Credentials are missing. Please Go to user and add the avalara credentials ")


class SaleOrderLineInherit(models.Model):
    _inherit = ["sale.order.line"]

    tran_service_type = fields.Many2one(related="product_id.product_tmpl_id.tran_service_type", string="TS Pairs")

    @api.model
    def _prepare_invoice_line(self):
        """
        Prepare the dict of values to create the new invoice line for a sales order line.

        :param qty: float quantity to invoice
        """
        self.ensure_one()
        return {
            "display_type": self.display_type,
            "sequence": self.sequence,
            "name": self.name,
            "product_id": self.product_id.id,
            "product_uom_id": self.product_uom.id,
            "quantity": self.qty_to_invoice,
            "discount": self.discount,
            "price_unit": self.price_unit,
            "tax_ids": [(6, 0, self.tax_id.ids)],
            "analytic_account_id": self.order_id.analytic_account_id.id,
            "analytic_tag_ids": [(6, 0, self.analytic_tag_ids.ids)],
            "sale_line_ids": [(4, self.id)],
            "tran_service_type": self.tran_service_type.id,
        }
