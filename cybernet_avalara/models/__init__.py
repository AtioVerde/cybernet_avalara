from . import account_move
from . import avalara_requests
from . import product_template
from . import res_partner
from . import res_users
from . import sale
from . import tax_line
