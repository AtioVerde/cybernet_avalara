from odoo import models, fields


class TaxLine(models.TransientModel):
    _name = "tax.line"
    _description = "Tax Line"

    tax_id = fields.Char()
    customer_name = fields.Char()
    tax_name = fields.Char()
    tax_amount = fields.Char()
    product_name = fields.Char()
    tax_level = fields.Char()
    date = fields.Datetime(string="Date-Time")
