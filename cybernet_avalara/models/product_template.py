from odoo import models, fields


class ProductTemplate(models.Model):
    _inherit = "product.template"

    tran_service_type = fields.Many2one("tran_service.avalara", "TS Pairs")
