import json
import base64
import requests
from odoo import fields, models, _
from odoo.osv import osv


class AvalaraTSPairs(models.Model):
    _name = "tran_service.avalara"
    _description = "Tran Service Avalara"

    t_code = fields.Char(string="Transaction Code")
    s_code = fields.Char(string="Service Code")
    name = fields.Char(string="TS Pairs")

    @staticmethod
    def get_tran_service(self):
        """
        THIS FUNCTION IS USED TO GET TRANSACTIONS FROM AFC
        :return:
        """
        try:
            tran_service_response = requests.get(
                "https://communications.avalara.net/api/v2/afc/tspairs",
                headers={
                    "Content-Type": "application/json",
                    "api_key": str(
                        base64.b64encode((str(self.env.user.email) + ":" + str(self.env.user.password)).encode()), "utf-8"
                    ),
                    "client_id": self.env.user.client_id,
                },
            )
            tspairs = json.loads(tran_service_response.text)
            for data in tspairs:
                trans_type = self.env["tran_service.avalara"].search([("name", "=", data["TSPairDescription"])])
                if trans_type:
                    trans_type.write(
                        {
                            "t_code": data["TransactionType"],
                            "s_code": data["ServiceType"],
                            "name": data["TSPairDescription"],
                        }
                    )
                else:
                    self.env["tran_service.avalara"].create(
                        {
                            "t_code": data["TransactionType"],
                            "s_code": data["ServiceType"],
                            "name": data["TSPairDescription"],
                        }
                    )

            self.env.cr.commit()
        except:
            raise osv.except_osv("", "While fetching or creating TS Pairs something went wrong.")
