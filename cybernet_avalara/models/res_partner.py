from odoo import models, fields


class Partner(models.Model):
    _inherit = "res.partner"

    customer_type = fields.Selection(
        [("0", "Residential"), ("1", "Business"), ("2", "Senior Citizen"), ("3", "Industrial")],
        default="1",
    )
