from odoo import models, fields, _
from odoo.addons.cybernet_avalara.models.avalara_requests import AvalaraTSPairs
from odoo.osv import osv


class Users(models.Model):
    _inherit = "res.users"

    email = fields.Char("Email Address", required=False)
    password = fields.Char(required=False)
    client_id = fields.Char("Client Id", required=False)
    business_class = fields.Selection([("0", "ILEC"), ("1", "CLEC")], default="1", string="Business Class")
    service_class = fields.Selection(
        [("0", "Primary Local"), ("1", "Primary Long Distance")], default="0", string="Service Class"
    )
    facilities = fields.Selection([("true", "True"), ("false", "False")], default="true", string="Facilities")
    franchise = fields.Selection([("true", "True"), ("false", "False")], default="true", string="Franchise")
    regulated = fields.Selection([("true", "True"), ("false", "False")], default="true", string="Regulated")
    company_name = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id.id)

    def test_connection(self):
        if self.client_id and self.email and self.password:
            if not self.env["account.tax"].search([("name", "=", "AFC Tax")]):
                self.env["account.tax"].create(
                    {
                        "name": "AFC Tax",
                        "type_tax_use": "sale",
                        "amount": 0.0,
                        "description": "Avalara for Communication Tax",
                    }
                )
            self.env.cr.commit()
            AvalaraTSPairs.get_tran_service(self)

            return self.action_of_button()
        else:
            raise osv.except_osv("", "Credentials are missing!")

    # def action_of_button(self):
    #     message_id = self.env["message.wizard"].create({"message": _("Successfully Connected to Avalara")})
    #     return {
    #         "name": _("Successfull"),
    #         "type": "ir.actions.act_window",
    #         "view_mode": "form",
    #         "res_model": "message.wizard",
    #         "res_id": message_id.id,
    #         "target": "new",
    #     }
