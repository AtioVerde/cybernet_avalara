{
    "name": "Avalara For Communications",
    "summary": """
        Odoo Avalara For Communications provides functionality to apply tax on transactions 
        from your AFC organization.""",
    "version": "14.0.1.0.0",
    "category": "Sale",
    "application": True,
    "depends": [
        "base",
        "sale_management",
        "sale",
    ],
    "description": """
        Avalara AvaTax for Communications connects to your current billing or ERP system
        to quickly determine and calculate taxes and fees for traditional telecommunication
        services, as well as VoIP, internet services, cable and satellite TV,
        and other communications services. By automating tax calculation and tracking the
        complex and ever-changing rules and rates for over 70,000 jurisdictions,
        Avalara saves you time, reduces your costs, and allows you to focus on business,
        not on tax obligations.
    """,
    "data": [
	"security/ir.model.access.csv",
        "views/account_move_views.xml",
        "views/cybernet_avalara_menu.xml",
        "views/product_template_views.xml",
        "views/res_partner_views.xml",
        "views/res_users_views.xml",
        "views/sale_views.xml",
        "views/tax_line_views.xml",
        "report/report_invoice.xml",
        "report/sale_report_templates.xml",
    ],
}
