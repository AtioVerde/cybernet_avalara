# -*- coding: utf-8 -*-
{
    'name': "Avalara For Communications",

    'summary': """
        Odoo Avalara For Communications provides functionality to apply tax on transactions from your AFC organization.""",

    'description': """
        Avalara AvaTax for Communications connects to your current billing or ERP system
        to quickly determine and calculate taxes and fees for traditional telecommunication
        services, as well as VoIP, internet services, cable and satellite TV,
        and other communications services. By automating tax calculation and tracking the
        complex and ever-changing rules and rates for over 70,000 jurisdictions, 
        Avalara saves you time, reduces your costs, and allows you to focus on business, 
        not on tax obligations.
    """,

    'author': "Techloyce",
    'website': "http://www.techloyce.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'images': [
        'static/description/banner.gif',
    ],
    'price': 499,
    'currency': 'EUR',
    'license' : 'OPL-1',
    'category': 'Sale',
    'version': '13.2.0',

    # any module necessary for this one to work correctly
    'depends': ['base','sale_management','sale'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}