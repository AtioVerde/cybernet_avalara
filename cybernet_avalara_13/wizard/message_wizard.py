from odoo import fields, models,api


class MessageWizard(models.TransientModel):
    _name = 'message.wizard'

    message = fields.Text('message', readonly=True)

    def action_ok(self):
        """ close wizard"""

        return {'type': 'ir.actions.act_window_close'}



