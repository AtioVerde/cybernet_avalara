from odoo import api, fields, models, _
from odoo.osv import osv
from odoo.exceptions import UserError
from odoo.tools.misc import formatLang, format_date
from datetime import datetime
import requests
import json
import base64


class AvalraConnection(models.Model):
    _inherit = 'res.users'

    email = fields.Char('Email Address', required=False)
    passw = fields.Char('Password', required=False)
    client_id = fields.Char('Client Id', required=False)
    business_class = fields.Selection([('0', 'ILEC'), ('1', 'CLEC')], default='1', string='Business Class')
    service_class = fields.Selection([('0', 'Primary Local'), ('1', 'Primary Long Distance')], default='0', string='Service Class')
    facilities = fields.Selection([('true', 'True'), ('false', 'False')], default='true', string='Facilities')
    franchise = fields.Selection([('true', 'True'), ('false', 'False')], default='true', string='Franchise')
    regulated = fields.Selection([('true', 'True'), ('false', 'False')], default='true', string='Regulated')
    company_name = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)

    def test_connection(self):
        if self.client_id and self.email and self.passw:
            if not self.env['account.tax'].search([('name', '=', 'AFC Tax')]):
                self.env['account.tax'].create({
                    'name': 'AFC Tax',
                    'type_tax_use': 'sale',
                    'amount': 0.0,
                    'description': 'Avalara for Communication Tax'
                })
            self.env.cr.commit()
            AvalaraTSPairs.get_tran_service(self)

            return self.action_of_button()
        else:
            raise osv.except_osv('', 'Credentials are missing!')

    def action_of_button(self):
        # do what ever login like in your case send an invitation
        ...
        ...
        # don't forget to add translation support to your message _()
        message_id = self.env['message.wizard'].create({'message': _("Successfully Connected to Avalara")})
        return {
            'name': _('Successfull'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'message.wizard',
            'res_id': message_id.id,
            'target': 'new',
        }


class AvalaraCustomers(models.Model):
    _inherit = 'res.partner'

    customer_type = fields.Selection([('0', 'Residential'), ('1', 'Business'), ('2', 'Senior Citizen'), ('3', 'Industrial')], default='1', string='Customer Type')


class AvalraProduct(models.Model):
    _inherit = 'product.template'

    tran_service_type = fields.Many2one('tran_service.avalara', 'TS Pairs')


class AvalaraInvoiceLine(models.Model):
    _inherit = 'account.move.line'

    tran_service_type = fields.Many2one(related='product_id.product_tmpl_id.tran_service_type', string='TS Pairs')


class AvalaraTSPairs(models.Model):
    _name = 'tran_service.avalara'

    t_code = fields.Char(string='Transaction Code')
    s_code = fields.Char(string='Service Code')
    name = fields.Char(string='TS Pairs')

    def get_tran_service(self):
        """
        THIS FUNCTION IS USED TO GET TRANSACTIONS FROM AFC
        :return:
        """
        try:

            tran_service_response = requests.get(
                'https://communications.avalara.net/api/v2/afc/tspairs',
                headers={
                    'Content-Type': 'application/json',
                    'api_key': str(base64.b64encode((str(self.env.user.email) + ':' + str(self.env.user.passw)).encode()),
                            'utf-8'),
                    'client_id': self.env.user.client_id
                })
            tspairs = json.loads(tran_service_response.text)
            for data in tspairs:
                trans_type = self.env['tran_service.avalara'].search([('name', '=', data['TSPairDescription'])])
                if trans_type:
                    trans_type.write({
                        't_code': data['TransactionType'],
                        's_code': data['ServiceType'],
                        'name': data['TSPairDescription'],
                       })
                else:
                    new_transaction = self.env['tran_service.avalara'].create({
                        't_code': data['TransactionType'],
                        's_code': data['ServiceType'],
                        'name': data['TSPairDescription'],
                    })

            self.env.cr.commit()
        except:
            raise osv.except_osv('', 'While fetching or creating TS Pairs something went wrong.')


class AvalaraInvoiceTax(models.Model):
    _inherit = ['account.move']

    isavalara = fields.Boolean(compute='_get_visible')
    sale_type = fields.Selection([('0', 'Wholesale'), ('1', 'Retail'), ('2', 'Consumed'), ('3', 'Vendor Use')], string='Sale Type')
    federal_tax = fields.Monetary('Federal Tax')
    state_tax = fields.Monetary('State Tax')
    county_tax = fields.Monetary('County Tax')
    city_tax = fields.Monetary('City Tax')
    unincorporated_tax = fields.Monetary('Unincorporated Tax')
    tax_line = fields.Many2many('tax.lines', 'tax_invoice_rel', 'order_id', 'tax_line_id', 'Tax Line')
    total_tax = total_amount_taxed_product = 0.0
    tax_document = {}
    invoice_document = {}
    customer_id = None
    invoice = []
    tax_line_list = []

    @api.onchange('invoice_line_ids')
    def _get_visible(self):
        self.isavalara = (self.env.company.id == self.env.user.company_name.id)

    @api.model
    @api.depends('line_ids.price_subtotal', 'line_ids.tax_base_amount', 'line_ids.tax_line_id', 'partner_id',
                 'currency_id')
    @api.onchange('invoice_line_ids', 'partner_id', 'sale_type')
    def _compute_invoice_taxes_by_group(self):
        ''' Helper to get the taxes grouped according their account.tax.group.
        This method is only used when printing the invoice.
        '''
        lines = []
        count = 0
        for move in self:
            if 'partner_id' in move:
                self.customer_id = move['partner_id'].id

            line_details = move.invoice_line_ids
            amount_untaxed = amount_tax = fed_t = sta_t = cou_t = cit_t = unin_t = 0.0
            self.tax_line_list.clear()
            for line in line_details:
                tax_ids = line.tax_ids
                for tax in tax_ids:
                    if tax.name == 'AFC Tax':
                        count += 1
                        lines.clear()
                        lines.append({
                            'chg': line.price_subtotal,
                            'line': count,
                            'sale': int(move.sale_type) if move.sale_type else "",
                            'tran': int(line.tran_service_type.t_code) if line.tran_service_type.t_code else "",
                            'serv': int(line.tran_service_type.s_code) if line.tran_service_type.s_code else "",
                        })
                        self.invoice_document['itms'] = lines
                        value = self.get_tax_avalara(line.name)
                        self.total_tax += float(value[0])
                        fed_t += float(value[1])
                        sta_t += float(value[2])
                        cou_t += float(value[3])
                        cit_t += float(value[4])
                        unin_t += float(value[5])

                        self.total_amount_taxed_product += line.price_subtotal
                        amount_tax += float(value[0])
                    else:
                        amount_tax += tax.amount
                amount_untaxed += line.price_subtotal

            self._tax_amount_all()
            self.update({
                'amount_untaxed': amount_untaxed,
                'amount_tax': amount_tax,
                'amount_total': amount_untaxed + amount_tax,
                'federal_tax': fed_t,
                'state_tax': sta_t,
                'county_tax': cou_t,
                'city_tax': cit_t,
                'unincorporated_tax': unin_t,
                'tax_line': [[6, 0, self.tax_line_list]] if self.tax_line_list else None
            })
            self.env.cr.commit()

            lang_env = move.with_context(lang=move.partner_id.lang).env
            tax_lines = move.line_ids.filtered(lambda line: line.tax_line_id)
            res = {}
            # There are as many tax line as there are repartition lines
            done_taxes = set()
            for line in tax_lines:
                res.setdefault(line.tax_line_id.tax_group_id, {'base': 0.0, 'amount': 0.0})
                res[line.tax_line_id.tax_group_id]['amount'] += line.price_subtotal
                tax_key_add_base = tuple(move._get_tax_key_for_group_add_base(line))
                if tax_key_add_base not in done_taxes:
                    # The base should be added ONCE
                    res[line.tax_line_id.tax_group_id]['base'] += line.tax_base_amount
                    done_taxes.add(tax_key_add_base)
            res = sorted(res.items(), key=lambda l: l[0].sequence)
            move.amount_by_group = [(
                group.name, amount_tax,
                amounts['base'],
                formatLang(lang_env, amount_tax, currency_obj=move.currency_id),
                formatLang(lang_env, amounts['base'], currency_obj=move.currency_id),
                len(res),
                group.id
            ) for group, amounts in res]

    @api.model
    def _get_tax_key_for_group_add_base(self, line):
        """
        Useful for _compute_invoice_taxes_by_group
        must be consistent with _get_tax_grouping_key_from_tax_line
         @return list
        """
        return [line.tax_line_id.id]
       
    def _tax_amount_all(self):
        if self.total_tax != 0.0 and self.total_amount_taxed_product != 0.0:
            tax_amount = (self.total_tax/self.total_amount_taxed_product)*100
            tax = self.env['account.tax'].search([('name', '=', 'AFC Tax')])
            tax.update({
                'name': 'AFC Tax',
                'type_tax_use': 'sale',
                'amount': tax_amount,
                'description': 'Avalara for Communication Tax'
            })
            self.env.cr.commit()

    def get_customer_detail(self):
        if self.customer_id:
            cus_model = self.env['res.partner']
            cus_rec_id = cus_model.search([('id', '=', self.customer_id)]).id

            customer = cus_model.browse(cus_rec_id)
            if customer.customer_type:
                self.invoice_document['cust'] = int(customer.customer_type)

            location = {
                'city': customer.city,
                'st': self.env['res.country.state'].search([('id', '=', int(customer.state_id))]).code,
                'zip': customer.zip,
                'ctry': self.env['res.country'].search([('id', '=', int(customer.country_id))]).code,
            }
            self.invoice_document['bill'] = location
        else:
            raise osv.except_osv('', 'Please Select Customer')

    def get_tax_avalara(self, p_name):
        if self.env.user.client_id and self.env.user.email and self.env.user.passw:
            try:
                self.get_customer_detail()
                self.invoice_document["date"] = str(datetime.now())
                self.invoice_document["cmmt"] = False
                self.invoice.append(self.invoice_document)
                self.tax_document["inv"] = self.invoice
                company_details = {
                    'bscl': int(self.env.user.business_class),
                    'svcl': int(self.env.user.service_class),
                    'frch': True if self.env.user.franchise == 'true' else False,
                    'fclt': True if self.env.user.facilities == 'true' else False,
                    'reg': True if self.env.user.regulated == 'true' else False,
                }
                self.tax_document['cmpn'] = company_details

                calculate_tax = requests.post('https://communications.avalara.net/api/v2/Afc/CalcTaxes',
                                              headers={'Content-Type': 'application/json',
                                                       'api_key': str(base64.b64encode((str(
                                                           self.env.user.email) + ':' + str(
                                                           self.env.user.passw)).encode()),
                                                                      'utf-8'),
                                                       'client_id': self.env.user.client_id
                                                       },
                                              data=json.dumps(self.tax_document))
                transaction_type = json.loads(calculate_tax.text)
                if len(transaction_type['inv'][0]['itms'][0]) != 0:
                    if 'txs' in transaction_type['inv'][0]['itms'][0]:
                        taxes = transaction_type['inv'][0]['itms'][0]['txs']
                        total_tax = 0
                        federal_t = 0
                        state_t = 0
                        county_t = 0
                        city_t = 0
                        unincorporated_t = 0
                        if self.tax_line:
                            self.update({
                                'tax_line': [[6, 0, []]]
                            })
                            self.tax_line.unlink()
                        for tax in taxes:
                            lvl = None
                            if tax['lvl'] == 0:
                                federal_t += tax['tax']
                                lvl = 'Federal'
                            elif tax['lvl'] == 1:
                                state_t += tax['tax']
                                lvl = 'State'
                            elif tax['lvl'] == 2:
                                county_t += tax['tax']
                                lvl = 'County'
                            elif tax['lvl'] == 3:
                                city_t += tax['tax']
                                lvl = 'City'
                            else:
                                unincorporated_t += tax['tax']
                                lvl = 'Unincorporated'

                            new_tax = self.env['tax.lines'].create({
                                'tax_id': tax['tid'],
                                'customer_name': self.partner_id.name,
                                'name': tax['name'],
                                'product_name': p_name,
                                'amount': tax['tax'],
                                'tax_level': lvl,
                                'date': datetime.now(),
                            })
                            self.tax_line_list.append(new_tax.id)
                        total_tax = federal_t + state_t + county_t + city_t + unincorporated_t

                        return total_tax, federal_t, state_t, county_t, city_t, unincorporated_t
                    else:
                        return 0
                else:
                    return 0
            except Exception as e:
                raise osv.except_osv('', e)
        else:
            raise osv.except_osv(" Credentials are missing. Please Go to user and add the avalara credentials ")


class SaleOrder(models.Model):
    _inherit = ['sale.order']

    isavalara = fields.Boolean(compute='_get_visible')
    sale_type = fields.Selection([('0', 'Wholesale'), ('1', 'Retail'), ('2', 'Consumed'), ('3', 'Vendor Use')],
                                 string='Sale Type')
    federal_tax = fields.Monetary('Federal Tax')
    state_tax = fields.Monetary('State Tax')
    county_tax = fields.Monetary('County Tax')
    city_tax = fields.Monetary('City Tax')
    unincorporated_tax = fields.Monetary('Unincorporated Tax')
    tax_line = fields.Many2many('tax.lines', 'tax_sale_rel', 'order_id', 'tax_line_id', 'Tax Line', ondelete='cascade')
    total_tax = total_amount_taxed_product = 0.0
    tax_document = {}
    invoice_document = {}
    customer_id = None
    invoice = []
    tax_line_list = []

    @api.onchange('order_line')
    def _get_visible(self):
        self.isavalara = (self.env.company.id == self.env.user.company_name.id)

    @api.model
    def _prepare_invoice(self):
        """
        Prepare the dict of values to create the new invoice for a sales order. This method may be
        overridden to implement custom invoice generation (making sure to call super() to establish
        a clean extension chain).
        """
        self.ensure_one()
        journal = self.env['account.move'].with_context(force_company=self.company_id.id,
                                                        default_type='out_invoice')._get_default_journal()

        if not journal:
            raise UserError(_('Please define an accounting sales journal for the company %s (%s).') % (
                self.company_id.name, self.company_id.id))

        invoice_vals = {
            'ref': self.client_order_ref or '',
            'type': 'out_invoice',
            'narration': self.note,
            'currency_id': self.pricelist_id.currency_id.id,
            'campaign_id': self.campaign_id.id,
            'medium_id': self.medium_id.id,
            'source_id': self.source_id.id,
            'invoice_user_id': self.user_id and self.user_id.id,
            'team_id': self.team_id.id,
            'partner_id': self.partner_invoice_id.id,
            'partner_shipping_id': self.partner_shipping_id.id,
            'fiscal_position_id': self.fiscal_position_id.id or self.partner_invoice_id.property_account_position_id.id,
            'invoice_origin': self.name,
            'invoice_payment_term_id': self.payment_term_id.id,
            'invoice_payment_ref': self.reference,
            'transaction_ids': [(6, 0, self.transaction_ids.ids)],
            'invoice_line_ids': [],
            'federal_tax': self.federal_tax,
            'state_tax': self.state_tax,
            'county_tax': self.county_tax,
            'city_tax': self.city_tax,
            'unincorporated_tax': self.unincorporated_tax,
            'sale_type': self.sale_type,
            'tax_line': [(6, 0, self.tax_line.ids)]
        }
        return invoice_vals

    @api.onchange('order_line', 'partner_id', 'sale_type')
    def get_ava_tax(self):
        """
        Compute the total amounts of the SO.
        """
        try:
            lines = []
            amount_untaxed = amount_tax = fed_t = sta_t = cou_t = cit_t = unin_t = 0.00
            for order in self:
                if 'partner_id' in order:
                    self.customer_id = order['partner_id'].id

                line_details = order.order_line
                self.tax_line_list.clear()
                for line in line_details:
                    tax_ids = line.tax_id

                    for tax in tax_ids:
                        if tax.name == 'AFC Tax':
                            lines.clear()
                            lines.append({
                                'chg': line.price_subtotal,
                                'sale': int(order.sale_type) if order.sale_type else "",
                                'tran': int(line.tran_service_type.t_code) if line.tran_service_type.t_code else "",
                                'serv': int(line.tran_service_type.s_code) if line.tran_service_type.s_code else "",
                                'qty': int(line.product_uom_qty) if line.product_uom_qty else None
                            })
                            self.invoice_document['itms'] = lines
                            value = self.get_tax_avalara(line.name)
                            self.total_tax += float(value[0])
                            fed_t += float(value[1])
                            sta_t += float(value[2])
                            cou_t += float(value[3])
                            cit_t += float(value[4])
                            unin_t += float(value[5])

                            self.total_amount_taxed_product += line.price_subtotal
                            amount_tax += float(value[0])
                        else:
                            amount_tax += line.tax_ids.amount
                    amount_untaxed += line.price_subtotal
                self.update({
                        'amount_untaxed': amount_untaxed,
                        'amount_tax': amount_tax,
                        'amount_total': amount_untaxed + amount_tax,
                        'federal_tax': fed_t,
                        'state_tax': sta_t,
                        'county_tax': cou_t,
                        'city_tax': cit_t,
                        'unincorporated_tax': unin_t,
                        'tax_line': [[6, 0, self.tax_line_list]] if self.tax_line_list else None
                    })
                self.env.cr.commit()
                self._tax_amount_all()
        except Exception as e:
            raise osv.except_osv('', e)

    def _tax_amount_all(self):
        if self.total_tax != 0.0 and self.total_amount_taxed_product != 0.0:
            tax_amount = (self.total_tax/self.total_amount_taxed_product)*100
            tax = self.env['account.tax'].search([('name', '=', 'AFC Tax')])
            tax.write({
                'name': 'AFC Tax',
                'type_tax_use': 'sale',
                'amount': tax_amount,
                'description': 'Avalara for Communication Tax'
            })
            self.env.cr.commit()

    def get_customer_detail(self):
        if self.customer_id:
            cus_model = self.env['res.partner']
            cus_rec_id = cus_model.search([('id', '=', self.customer_id)]).id

            customer = cus_model.browse(cus_rec_id)
            if customer.customer_type:
                self.invoice_document['cust'] = int(customer.customer_type)

            location = {
                'city': customer.city,
                'st': self.env['res.country.state'].search([('id', '=', int(customer.state_id))]).code,
                'zip': customer.zip,
                'ctry': self.env['res.country'].search([('id', '=', int(customer.country_id))]).code,
            }
            self.invoice_document['bill'] = location
        else:
            raise osv.except_osv('', 'Please Select Customer')

    def get_tax_avalara(self, p_name):
        if self.env.user.client_id and self.env.user.email and self.env.user.passw:
            try:
                if self.env.user.client_id and self.env.user.email and self.env.user.passw:
                    self.get_customer_detail()
                    self.invoice_document["date"] = str(datetime.now())
                    self.invoice_document["cmmt"] = False
                    self.invoice.append(self.invoice_document)
                    self.tax_document["inv"] = self.invoice
                    company_details = {
                        'bscl': int(self.env.user.business_class),
                        'svcl': int(self.env.user.service_class),
                        'frch': True if self.env.user.franchise == 'true' else False,
                        'fclt': True if self.env.user.facilities == 'true' else False,
                        'reg': True if self.env.user.regulated == 'true' else False,
                    }
                    self.tax_document['cmpn'] = company_details

                    calculate_tax = requests.post('https://communications.avalara.net/api/v2/Afc/CalcTaxes',
                                                  headers={'Content-Type': 'application/json',
                                                           'api_key': str(base64.b64encode((str(self.env.user.email) + ':' + str(self.env.user.passw)).encode()),
                                                                          'utf-8'),
                                                           'client_id': self.env.user.client_id
                                                           },
                                                  data=json.dumps(self.tax_document))
                    transaction_type = json.loads(calculate_tax.text)
                    if len(transaction_type['inv'][0]['itms'][0]) != 0:
                        if 'txs' in transaction_type['inv'][0]['itms'][0]:
                            taxes = transaction_type['inv'][0]['itms'][0]['txs']
                            total_tax = 0
                            federal_t = 0
                            state_t = 0
                            county_t = 0
                            city_t = 0
                            unincorporated_t = 0
                            if self.tax_line:
                                self.update({
                                    'tax_line': [[6, 0, []]]
                                })
                                self.tax_line.unlink()
                            for tax in taxes:
                                lvl = None
                                if tax['lvl'] == 0:
                                    federal_t += tax['tax']
                                    lvl = 'Federal'
                                elif tax['lvl'] == 1:
                                    state_t += tax['tax']
                                    lvl = 'State'
                                elif tax['lvl'] == 2:
                                    county_t += tax['tax']
                                    lvl = 'County'
                                elif tax['lvl'] == 3:
                                    city_t += tax['tax']
                                    lvl = 'City'
                                else:
                                    unincorporated_t += tax['tax']
                                    lvl = 'Unincorporated'

                                new_tax = self.env['tax.lines'].create({
                                    'tax_id': tax['tid'],
                                    'customer_name': self.partner_id.name,
                                    'name': tax['name'],
                                    'product_name': p_name,
                                    'amount': tax['tax'],
                                    'tax_level': lvl,
                                    'date': datetime.now(),
                                })
                                self.tax_line_list.append(new_tax.id)
                            total_tax = federal_t + state_t + county_t + city_t + unincorporated_t

                            return total_tax, federal_t, state_t, county_t, city_t, unincorporated_t
                        else:
                            return 0
                    else:
                        return 0
            except Exception as e:
                raise osv.except_osv('', e)
        else:
            raise osv.except_osv(" Credentials are missing. Please Go to user and add the avalara credentials ")


class SaleOrderLineInherit(models.Model):
    _inherit = ['sale.order.line']

    tran_service_type = fields.Many2one(related='product_id.product_tmpl_id.tran_service_type', string='TS Pairs')

    @api.model
    def _prepare_invoice_line(self):
        """
        Prepare the dict of values to create the new invoice line for a sales order line.

        :param qty: float quantity to invoice
        """
        self.ensure_one()
        return {
            'display_type': self.display_type,
            'sequence': self.sequence,
            'name': self.name,
            'product_id': self.product_id.id,
            'product_uom_id': self.product_uom.id,
            'quantity': self.qty_to_invoice,
            'discount': self.discount,
            'price_unit': self.price_unit,
            'tax_ids': [(6, 0, self.tax_id.ids)],
            'analytic_account_id': self.order_id.analytic_account_id.id,
            'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
            'sale_line_ids': [(4, self.id)],
            'tran_service_type': self.tran_service_type.id
        }


class TaxLines(models.TransientModel):
    _name = 'tax.lines'

    tax_id = fields.Char('Tax Id')
    customer_name = fields.Char('Customer Name')
    name = fields.Char('Tax Name')
    amount = fields.Char('Tax Amount')
    product_name = fields.Char('Product Name')
    tax_level = fields.Char('Tax Level')
    date = fields.Datetime(string='Date-Time')
